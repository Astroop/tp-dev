<?php

namespace App\Controller;

use App\Entity\PostCategory;
use App\Form\PostCategoryType;
use App\Repository\PostCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/post/category")
 */
class PostCategoryController extends AbstractController
{
    /**
     * @Route("/", name="post_category_index", methods={"GET"})
     */
    public function index(PostCategoryRepository $postCategoryRepository): Response
    {
        return $this->render('post_category/index.html.twig', [
            'post_categories' => $postCategoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="post_category_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $postCategory = new PostCategory();
        $form = $this->createForm(PostCategoryType::class, $postCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($postCategory);
            $entityManager->flush();

            return $this->redirectToRoute('post_category_index');
        }

        return $this->render('post_category/new.html.twig', [
            'post_category' => $postCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_category_show", methods={"GET"})
     */
    public function show(PostCategory $postCategory): Response
    {
        return $this->render('post_category/show.html.twig', [
            'post_category' => $postCategory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="post_category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PostCategory $postCategory): Response
    {
        $form = $this->createForm(PostCategoryType::class, $postCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('post_category_index');
        }

        return $this->render('post_category/edit.html.twig', [
            'post_category' => $postCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PostCategory $postCategory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$postCategory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($postCategory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_category_index');
    }
}
