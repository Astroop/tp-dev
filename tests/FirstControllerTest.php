<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testSomething()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Bienvenue sur la page d\'accueil', $crawler->filter('h1')->text());
    }

    /**
     * @dataProvider provideUrls
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function provideUrls()
    {
        return [
            ['/'],
            ['/login'],
            ['/post']
        ];
    }

    public function testClickAjouter() {

        $client = static::createClient();
        $crawler = $client->request('GET', '/');
   
        $link = $crawler
            ->filter('a:contains("Login")')
            ->link();
   
        $crawler = $client->click($link);
        $this->assertSame(200, $client->getResponse()->getStatusCode());
   
    }

    public function testLoginFormulaire() {

        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
   
       $form = $crawler->selectButton('Sign in')->form();
   
       $form['email'] = 'test@gmail.com';
       $form['password'] = 'password';
   
       $crawler = $client->submit($form);
       $this->assertSame(302, $client->getResponse()->getStatusCode());
   
    }
}